#ifndef ORDERS_PARSER_CLASS_H_
#define ORDERS_PARSER_CLASS_H_


#include <yaml-cpp/yaml.h>
#include <string>
#include <iostream>
#include <assert.h>
#include <thread>
#include <filesystem>
#include <mutex>

typedef struct Order
{
    /* Order data */
    uint32_t order_id;
    float cx;
    float cy;
    std::vector<int> products;
} Order;

/*--------------------------------------*/

class OrdersParser
{
    
private:
    /* data */
    std::unordered_map<uint32_t, Order> orders_database;

    std::vector<std::filesystem::path> orders_files_paths;
    std::mutex m_unordered_map;

public:
    void friend operator>>(const YAML::Node &node, Order &current_order);

    // Getting all yaml files paths in the order folder
    std::vector<std::filesystem::path> getFilesSystemPaths(std::string_view data_folder_path);

    // Parse Orders from yaml file configuration
    bool parseOrdersFromFile(std::string orders_filepath);

    // Parse n yaml files using n threads in the order folder
    bool readOrdersSimultaneously(std::string_view orders_folder_path);

    // Retrieve order form orders parsed database
    Order &getOrderFromDatabase(uint32_t order_id) { return orders_database.at(order_id); };

    // Get information about orders database
    void displayInfo(){
            std::cout << "Orders Database has: " 
                      << orders_database.size()      
                      << " Unique Order"<< "\n";        
    }
    
    // Display files paths
    void displayFilePath(){
        for(auto path : orders_files_paths){
            std::cout << path.string() << "\n";
        }
    }
};

/*--------------------------------------*/
#endif