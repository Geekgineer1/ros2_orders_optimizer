#ifndef PRODUCTS_PARSER_CLASS_H_
#define PRODUCTS_PARSER_CLASS_H_


#include <yaml-cpp/yaml.h>
#include <string>
#include <iostream>
#include <assert.h>
#include <thread>
#include <filesystem>

/*--------------------------------------*/

/* Custom data types */
typedef struct Part
{
    /* Part data */
    std::string part_name;
    float cx;
    float cy;
} Part;

typedef struct Product
{
    /* Product data */
    int id;
    std::string product_name;
    std::vector<Part> parts;
} Product;

/*--------------------------------------*/

class ProductsParser
{
private:
    /* data */
    std::vector<Product> products_configurations;
    std::string products_config_filepath;

public:

    void friend operator>>(const YAML::Node &node, Product &current_product);
    bool parseProductsFromFile(std::string &config_filepath);

    Product &getProductFromDatabase(int product_id) { 
            // since the products ids is sorted ints, return product by indx
            return products_configurations[product_id - 1]; };

    void displayFilePath(){std::cout << products_config_filepath << "\n";}
};

#endif