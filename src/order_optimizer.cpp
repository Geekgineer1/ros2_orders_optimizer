
#include <chrono>
#include <random>

#include "geometry_msgs/msg/pose_stamped.hpp"
#include "include/orders_parser.hpp"
#include "include/products_parser.hpp"
#include "orders_opt/msg/order.hpp"
#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "visualization_msgs/msg/marker_array.hpp"

using namespace std::placeholders;
using namespace std::chrono_literals;

/**
 * Estimate the elapsed time
 *
 */

template <
    class result_t = std::chrono::seconds,
    class clock_t = std::chrono::steady_clock,
    class duration_t = std::chrono::seconds>
inline auto since(std::chrono::time_point<clock_t, duration_t> const& start) {
    return std::chrono::duration_cast<result_t>(clock_t::now() - start);
}

class OrderOptimizer : public rclcpp::Node {
   public:
    // initiate the node in constructor with a name and options
    OrderOptimizer() : Node("OrderOptimizer") {
        setvbuf(stdout, NULL, _IONBF, BUFSIZ);

        /**
         * Get product list and populate database
         *
         */

        this->declare_parameter<std::string>("config_path");

        rclcpp::Parameter config_path_prm = this->get_parameter("config_path");

        config_pth = config_path_prm.as_string() + "/configuration/products.yaml";
        orders_pth = config_path_prm.as_string() + "/orders";

        product_parser.parseProductsFromFile(config_pth);

        /**
         * 2 Subscribers 1 publisher
         * nextOrder       : topic for receiving the order from customer
         * currentPosition : topic for receiving the order robot current position
         * order_path      : topic for publishing Rviz markers the order
         *
         */

        nextOrder_sub = this->create_subscription<orders_opt::msg::Order>(
            "nextOrder", 10, std::bind(&OrderOptimizer::orderCallback, this, _1));

        currentPosition_sub = this->create_subscription<geometry_msgs::msg::PoseStamped>(
            "currentPosition", 10, std::bind(&OrderOptimizer::positionCallback, this, _1));

        order_path_pub = this->create_publisher<visualization_msgs::msg::MarkerArray>("order_path", 10);
    }

   private:
    void positionCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg);

    void orderCallback(const orders_opt::msg::Order::SharedPtr msg);

    std::vector<std::pair<int, Part>> getNearestPickupParts(std::vector<std::pair<int, Part>>& parts_to_pick,
                                                            double& crr_robot_cx,
                                                            double& crr_robot_cy);

    visualization_msgs::msg::Marker toMarker(const std::string& frame,
                                             const std::string& type,
                                             const double& scale,
                                             const std_msgs::msg::ColorRGBA& unique_color,
                                             const double& cx,
                                             const double& cy);

    visualization_msgs::msg::Marker toTextMarker(const std::string& frame,
                                                 const double& scale,
                                                 const std::string& description,
                                                 const double& cx,
                                                 const double& cy);

    std::string config_pth = "";
    std::string orders_pth = "";

    ProductsParser product_parser;

    geometry_msgs::msg::PoseStamped robot_pose_stamped;

    rclcpp::Subscription<orders_opt::msg::Order>::SharedPtr nextOrder_sub;
    rclcpp::Subscription<geometry_msgs::msg::PoseStamped>::SharedPtr currentPosition_sub;
    rclcpp::Publisher<visualization_msgs::msg::MarkerArray>::SharedPtr order_path_pub;
};

/**
 * RViz Visualization Utilities
 *
 */

// marker generation representing 3d shape
visualization_msgs::msg::Marker OrderOptimizer::toMarker(const std::string& frame,
                                                         const std::string& type,
                                                         const double& scale,
                                                         const std_msgs::msg::ColorRGBA& unique_color,
                                                         const double& cx,
                                                         const double& cy) {
    visualization_msgs::msg::Marker marker;

    // set marker shape and parameters
    if (type == "cube") {
        marker.type = visualization_msgs::msg::Marker::CUBE;
    } else if (type == "sphere") {
        marker.type = visualization_msgs::msg::Marker::SPHERE;
    } else if (type == "cylinder") {
        marker.type = visualization_msgs::msg::Marker::CYLINDER;
    } else if (type == "line_strip") {
        marker.type = visualization_msgs::msg::Marker::LINE_STRIP;
    } else if (type == "sphere_list") {
        marker.type = visualization_msgs::msg::Marker::SPHERE_LIST;
    } else if (type == "points") {
        marker.type = visualization_msgs::msg::Marker::POINTS;
    } else {
        marker.type = visualization_msgs::msg::Marker::CUBE;
    }

    marker.id = 5 + (std::rand() % (100000 - 5 + 1));

    marker.header.frame_id = frame;
    marker.header.stamp = this->now();
    marker.ns = this->get_namespace();

    marker.pose.position.x = cx / 100.;
    marker.pose.position.y = cy / 100.;
    marker.pose.position.z = 0.0;

    marker.pose.orientation.x = 0.;
    marker.pose.orientation.y = 0.;
    marker.pose.orientation.z = 0.;
    marker.pose.orientation.w = 1.;

    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;

    marker.color = unique_color;

    marker.action = visualization_msgs::msg::Marker::ADD;
    marker.lifetime = rclcpp::Duration::from_seconds(60);

    return marker;
}

// marker generation representing text view
visualization_msgs::msg::Marker OrderOptimizer::toTextMarker(const std::string& frame,
                                                             const double& scale,
                                                             const std::string& description,
                                                             const double& cx,
                                                             const double& cy) {
    visualization_msgs::msg::Marker marker;

    // set marker shape and parameters
    marker.type = visualization_msgs::msg::Marker::TEXT_VIEW_FACING;

    marker.text = description;
    marker.id = 5 + (std::rand() % (100000 - 5 + 1));

    marker.header.frame_id = frame;
    marker.header.stamp = this->now();
    marker.ns = this->get_namespace();

    marker.pose.position.x = cx / 100.;
    marker.pose.position.y = cy / 100.;
    marker.pose.position.z = 1.0;

    marker.pose.orientation.x = 0.;
    marker.pose.orientation.y = 0.;
    marker.pose.orientation.z = 0.;
    marker.pose.orientation.w = 1.;

    marker.scale.x = scale;
    marker.scale.y = scale;
    marker.scale.z = scale;

    marker.color.r = 1.0;
    marker.color.g = 1.0;
    marker.color.b = 1.0;
    marker.color.a = 1.0;

    marker.action = visualization_msgs::msg::Marker::ADD;
    marker.lifetime = rclcpp::Duration::from_seconds(60);

    return marker;
}

/**
 * Robot postion
 *
 */

// set the robot position externally
void OrderOptimizer::positionCallback(const geometry_msgs::msg::PoseStamped::SharedPtr msg) {
    robot_pose_stamped.pose = msg->pose;
    robot_pose_stamped.header = msg->header;

    RCLCPP_INFO(this->get_logger(), "Robot position updated to x: '%f' , y: '%f' ",
                robot_pose_stamped.pose.position.x,
                robot_pose_stamped.pose.position.y);
}

/**
 * Logic of finding the shortest path from the robot to part
 *
 */

// get the cost of travel to all parts as direct line of sight distance
std::vector<std::pair<int, Part>> OrderOptimizer::getNearestPickupParts(std::vector<std::pair<int, Part>>& parts_to_pick,
                                                                        double& crr_robot_cx, double& crr_robot_cy) {
    double travel_cost_min = std::numeric_limits<double>::infinity();

    std::vector<std::pair<int, Part>> nearest_parts_pairs;

    // Get min travel cost to all parts
    for (auto part_pair : parts_to_pick) {
        double travel_cost = hypot(crr_robot_cx - part_pair.second.cx, crr_robot_cy - part_pair.second.cy);
        if (travel_cost_min > travel_cost) travel_cost_min = travel_cost;
    }

    // Get parts with min travel cost
    for (int i = parts_to_pick.size() - 1; i >= 0; i--) {
        double part_travel_cost = hypot(crr_robot_cx - parts_to_pick.at(i).second.cx,
                                        crr_robot_cy - parts_to_pick.at(i).second.cy);

        if (part_travel_cost <= travel_cost_min) {
            nearest_parts_pairs.push_back(parts_to_pick.at(i));
            parts_to_pick.erase(parts_to_pick.begin() + i);
        }
    }

    nearest_parts_pairs.shrink_to_fit();

    return nearest_parts_pairs;
}

/**
 * Core order handling and finding the parts of each product in the order
 *
 */

void OrderOptimizer::orderCallback(const orders_opt::msg::Order::SharedPtr msg)

{
    RCLCPP_INFO(this->get_logger(), "Received customer order number: %d", msg->order_id);
    RCLCPP_INFO(this->get_logger(), "Received customer order description: %s", msg->description.c_str());

    /*---------------------------------*/
    // Load orders database based on files
    OrdersParser order_parser;

    RCLCPP_INFO(this->get_logger(), "Loading orders database from: %s", orders_pth.c_str());
    auto start = std::chrono::steady_clock::now();
    order_parser.readOrdersSimultaneously(orders_pth);
    RCLCPP_INFO(this->get_logger(), "Orders database loaded took(ms): %ld", since(start).count());

    Order ordered = order_parser.getOrderFromDatabase(msg->order_id);

    /*---------------------------------*/
    // collect parts from a given order
    std::vector<std::pair<int, Part>> parts_to_pick;

    for (auto& product_idx : ordered.products) {
        Product ordered_product = product_parser.getProductFromDatabase(product_idx);

        for (auto& part : ordered_product.parts) {
            parts_to_pick.push_back(std::make_pair(ordered_product.id, part));
        }
    }

    parts_to_pick.shrink_to_fit();

    /*---------------------------------*/
    int num_parts = parts_to_pick.size();
    RCLCPP_INFO(this->get_logger(), "Count of parts to pick in the requested order: %i", num_parts);

    /*---------------------------------*/
    // get robot pos
    double crr_robot_cx = robot_pose_stamped.pose.position.x;
    double crr_robot_cy = robot_pose_stamped.pose.position.y;

    /*---------------------------------*/
    // Process the parts in order

    for (auto i = 0; i < num_parts; i++) {
        std::vector<std::pair<int, Part>> crr_parts_pairs = getNearestPickupParts(parts_to_pick,
                                                                                  crr_robot_cx, crr_robot_cy);

        // assuming robot reached current parts location [will be removed later once received actual odom]
        crr_robot_cx = crr_parts_pairs[0].second.cx;
        crr_robot_cy = crr_parts_pairs[0].second.cy;

        visualization_msgs::msg::MarkerArray crr_marker_array;

        // collect robot current location colored marker
        std_msgs::msg::ColorRGBA robot_marker_color;  // red
        robot_marker_color.r = 1.;
        robot_marker_color.g = 0.;
        robot_marker_color.b = 0.;
        robot_marker_color.a = 1.;

        crr_marker_array.markers.push_back(toMarker("map", "cube", 0.5,
                                                    robot_marker_color,
                                                    crr_robot_cx,
                                                    crr_robot_cy));

        // collect parts current location colored markers
        std_msgs::msg::ColorRGBA part_marker_color;  // cyan
        part_marker_color.r = 0.;
        part_marker_color.g = 1;
        part_marker_color.b = 0.;
        part_marker_color.a = 1.;

        for (auto product_part : crr_parts_pairs) {
            crr_marker_array.markers.push_back(toMarker("map", "cylinder", 0.5,
                                                        part_marker_color,
                                                        product_part.second.cx,
                                                        product_part.second.cy));

            crr_marker_array.markers.push_back(toTextMarker("map", 0.4,
                                                            product_part.second.part_name,
                                                            product_part.second.cx,
                                                            product_part.second.cy));
        }

        RCLCPP_INFO(this->get_logger(), "Markers Published");

        // publish the markers
        order_path_pub->publish(crr_marker_array);

        // give the markers some times
        rclcpp::sleep_for(3s);

        // pick the part and remove it from parts list
        for (auto product_part : crr_parts_pairs) {
            RCLCPP_INFO(this->get_logger(), "Fetching part '%s' for product '%i' at x: '%f' , y: '%f' ",
                        product_part.second.part_name.c_str(),
                        product_part.first,
                        product_part.second.cx,
                        product_part.second.cy);

            RCLCPP_INFO(this->get_logger(), "Picked part for product: '%i' with part name '%s'",
                        product_part.first,
                        product_part.second.part_name.c_str());
        }

        if (parts_to_pick.empty()) {
            RCLCPP_INFO(this->get_logger(), "Fetching parts is done! waiting the next order");
            break;
        }

        RCLCPP_INFO(this->get_logger(), "Moving to next parts location");
    }
}

int main(int argc, char** argv) {
    // initiate ROS2 communications
    rclcpp::init(argc, argv);

    // create node handled by smart pointer which performs a single heap-allocation
    auto node = std::make_shared<OrderOptimizer>();

    // keep the node alive
    rclcpp::spin(node);

    // stop ROS2 communications upon killing the node
    rclcpp::shutdown();
}