// incorporate file that defines the class "ProductParser"
#include "include/products_parser.hpp"

/*--------------------------------------*/
// extraction operator, maps YAML Node data fields to Order data fields
void operator>>(const YAML::Node &node, Product &current_product)
{
    current_product.id = node["id"].as<int>();
    current_product.product_name = node["product"].as<std::string>();

    // retrieve a sub-node by querying the root 'map node' using a key value
    const YAML::Node &parts = node["parts"];

    if (YAML::Node parts = node["parts"])
    {
        // iterate through all parts elements
        // int index = 0;

        // process parts
        for (YAML::const_iterator it = parts.begin(); it != parts.end(); ++it)
        {
            // get reference to the "it" points and create new YAML::Node reference out of it
            const YAML::Node &part = *it;

            Part current_part;

            current_part.part_name = part["part"].as<std::string>();
            current_part.cx = part["cx"].as<float>();
            current_part.cy = part["cy"].as<float>();
            
            // collect part
            current_product.parts.push_back(current_part);
        }
    }
}

/*--------------------------------------*/
// Parse Products from yaml file configuration, feed to vector of Products
bool ProductsParser::parseProductsFromFile(std::string &config_filepath)
{
    products_config_filepath = config_filepath;

    std::vector<YAML::Node> yaml_docs;

    try
    {
        // Load the YAML file into vector that contains all yaml documents
        yaml_docs = YAML::LoadAllFromFile(products_config_filepath);
    }
    catch (YAML::BadFile &ex)

    {
        std::cout << "ERROR!" << '\n';
        std::cout << ex.what() << '\n';
        return false;
    }

    int total_counter = 0;

    // sequence iterator to iterate through all documents in the file
    for (const auto &yaml_doc : yaml_docs)
    {
        // map iterator to iterate through all products in document
        for (YAML::const_iterator it = yaml_doc.begin(); it != yaml_doc.end(); ++it)
        {
            // get reference to the "it" yaml_doc and create new YAML::Node reference out of it
            const YAML::Node &node = *it;

            // declare variable used to keep each product
            Product current_product;

            // map all YAML Node data fields to Product data fields using a custom extraction operator
            node >> current_product;

            // add current product to vector of parsed products
            products_configurations.push_back(current_product);

            // std::cout << "done current_product" << total_counter << '\n';
            total_counter++;
        }
    }

    return true;
}
/*--------------------------------------*/