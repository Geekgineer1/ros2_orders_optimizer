// incorporate file that defines the class "ProductParser"
#include "include/orders_parser.hpp"

// extraction operator, maps YAML Node data fields to Order data fields
void operator>>(const YAML::Node &node, Order &current_order)
{
    current_order.order_id = node["order"].as<uint32_t>();
    current_order.cx = node["cx"].as<float>();
    current_order.cy = node["cy"].as<float>();
    current_order.products = node["products"].as<std::vector<int>>();
}

// Parse Orders from yaml file configuration, feed map of Orders to hold the result
bool OrdersParser::parseOrdersFromFile(std::string order_filepath)
{
    std::vector<YAML::Node> yaml_docs;

    try
    {
        // Load the YAML file into vector that contains all yaml documents
        yaml_docs = YAML::LoadAllFromFile(order_filepath);
    }
    catch (YAML::BadFile &ex)

    {
        std::cout << "ERROR!" << '\n';
        std::cout << ex.what() << '\n';
        return false;
    }

    int total_counter = 0;

    // sequence iterator to iterate through all documents in the file
    for (const auto &yaml_doc : yaml_docs)
    {
        // map iterator to iterate through all Orders in document
        for (YAML::const_iterator it = yaml_doc.begin(); it != yaml_doc.end(); ++it)
        {
            // get reference to the "it" yaml_doc and create new YAML::Node reference out of it
            const YAML::Node &node = *it;

            // declare Order variable used to keep each order
            Order current_order;

            // map all YAML Node data fields to Order data fields using a custom extraction operator
            node >> current_order;

            // safe thread access to database represented by map
            m_unordered_map.lock();

            // add current product to vector of parsed products
            // auto [itr, ins] = orders_database.insert_or_assign(current_order.order_id, current_order);
            // assert(ins == false);
            orders_database.insert_or_assign(current_order.order_id, current_order);

            m_unordered_map.unlock();

            total_counter++;
        }
        //  std::cout << "loading done : "<< total_counter << '\n';
    }

    return true;
}

// Retrieve all paths from directory
std::vector<std::filesystem::path> OrdersParser::getFilesSystemPaths(std::string_view data_folder_path)
{

    std::vector<std::filesystem::path> files_in_directory;

    for (const auto &entry : std::filesystem::directory_iterator(data_folder_path))
        files_in_directory.push_back(entry.path());

    // sort files in ascending order so loading is chronological
    sort(files_in_directory.begin(), files_in_directory.end());

    return files_in_directory;
}

// Retrieve and parse all files from directory into map database
bool OrdersParser::readOrdersSimultaneously(std::string_view orders_folder_path)
{
    // Files Paths
    orders_files_paths = getFilesSystemPaths(orders_folder_path);

    // One thread per file
    int num_threads = orders_files_paths.size();

    std::vector<std::thread> threads(num_threads);

    // Spawn n threads for n files in folder
    for (int i = 0; i < num_threads; i++)
    {
        /* assign 1 file to be processed by 1 thread  */
        threads[i] = std::thread(&OrdersParser::parseOrdersFromFile, this, orders_files_paths[i].string());
    }

    for (auto &th : threads)
    {
        /* execute */
        th.join();
    }

    return true;
}
/*--------------------------------------*/