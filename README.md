
# ros2_orders_optimizer


![ROS Package Index](https://img.shields.io/ros/v/humble/vision_msgs?style=flat-square)
![Gitlab pipeline status (self-managed)](https://img.shields.io/gitlab/pipeline-status/Geekgineer1/ros2_orders_optimizer?branch=main&style=flat-square)


AMR Picking parts from order for ROS 2. (Evaluation Package)

## Overview

1. On each order that is received via the ‘nextOrder’ topic, the node should parse all files in the folder ‘orders’ to find information for that given order. Due to performance reasons, parsing of multiple files must be done in separate threads, one per file. For the purpose
of this fictional example we assume that the files change regularly, so parsing them each time is required, although we are aware that this is neither a good idea nor best practice.

2. After obtaining the products required for an order, your node should check which parts are required for the respective products using the yaml file in the folder ‘configuration’.
We assume that this configuration does not change regularly, so you may parse it once when starting the node.

3. Having obtained all products and necessary parts for a given order, your node should
determine the geometrically shortest path that the AMR needs to travel to collect all parts.
We assume a completely empty map without any obstacles for this purpose. The current
position of the AMR should be considered when calculating this shortest path.

4. The node should then also publish a marker array of the message type ‘visualiza-
tion_msgs/MarkerArray’ on the topic ‘order_path’ where the AMR position itself is indi-
cated with a marker of type ‘CUBE’ while each part pickup location should be visualized
with a marker of type ‘CYLINDER’


## Building

Developed using ROS 2 Humble on Ubuntu 20.04 (Focal): amd64, these instructions also apply to other ros2 distro and no requirement for other external libs; it should work out-of-the-box.



1. Build assuming you've already have ROS 2 installed :

    ```
    $ mkdir -p amr_ws/src && cd ~/amr_ws/src
    $ git clone https://gitlab.com/Geekgineer1/ros2_orders_optimizer.git
    $ cd ~/amr_ws
    $ colcon build --packages-select orders_opt
    ```

2. Source and check:
   
    ```
    $ . install/setup.bash
    
    check the order custom msg 
    $ ros2 interface list -m |grep Order
    ```
## Running:

*   In terminal 1:
    ```
    $ . install/setup.bash
    
    check the order custom msg 
    $ ros2 interface list -m |grep Order
    ```

*   In terminal 2:
    ```
    $ cd ~/amr_ws && source /opt/ros/humble/setup.bash

    $ . install/setup.bash

    $  ros2 run rviz2 rviz2 
    
    ```

*   In terminal 3:
    ```
    $ cd ~/amr_ws && source /opt/ros/humble/setup.bash

    $ . install/setup.bash

    $  ros2 run tf2_ros static_transform_publisher "0" "0" "0" "0" "0" "0" "world" "map"
    
    In Rviz set fixed frame to map and add markers array display also set the topic to /order_path
    ```

*   In terminal 4:
    ```
    $ cd ~/amr_ws && source /opt/ros/humble/setup.bash

    $ . install/setup.bash

    $  ros2 run orders_opt order_optimizer --ros-args -p config_path:="/home/$USER/amr_ws/src/orders_opt/config"
    
    ```


*   In terminal 5: 
    ```
    $ cd ~/amr_ws && source /opt/ros/humble/setup.bash

    $ . install/setup.bash

    $  ros2 topic pub --once /currentPosition geometry_msgs/PoseStamped "{header: {stamp: {sec: 0, nanosec: 0}, frame_id: "map"}, pose: {position: {x: 1.0, y: 0.0, z: 0.0}, orientation: {x: 1.0, y: 0.0, z: 0.0, w: 1.0}}}"
    
    ```

*   In terminal 6: 
    ```
    $ cd ~/amr_ws && source /opt/ros/humble/setup.bash

    $ . install/setup.bash

    $  ros2 topic pub --once /nextOrder orders_opt/msg/Order "{order_id: 1200002 , description: 'testing with one customer order'}"
    
    ```

## Results:

### Info output from terminal
![terminal info](./img/output_results.png)

### Rviz output 
![rviz part](./img/rviz_parts.png)